import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';  
import { Observable } from 'rxjs/Rx';
import { Router } from '@angular/router';


import 'rxjs/add/operator/map'; //map
import 'rxjs/add/operator/catch'; //catch

@Injectable()
export class ServiceService {

  constructor(private http:Http) {   }

    getData()
      {
    
        return this.http
        .get('http://localhost:8000/api/mycourse')

        .map(result => result.json()) //map untuk mengconvert json ke object
        .catch(error => Observable.throw(error.json().error) || "Server Error");  //catch untuk menghendle error

      }

    addData(nama_course: string, jadwal: string )
      {
        let data = 
          {
            "nama_ourse" : nama_course,
            "jadwal" : jadwal,
          };

        let body = JSON.stringify(data);
        let headers = new Headers
          ({ 
            "Content-Type" : "application/json"});
        let options = new RequestOptions({ headers : headers });

         return this.http
        .get('http://localhost:8000/api/mycourse/add')
      } 

  

}
