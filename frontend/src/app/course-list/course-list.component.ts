import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../service.service';

@Component({
  selector: 'app-course-list',
  templateUrl: './course-list.component.html',
  styleUrls: ['./course-list.component.css']
})
export class CourseListComponent implements OnInit {

   courseList: Object[];

   newname:string = "";
   newjadwal:string = "";
  

  constructor(private service:ServiceService) { 

    
  }

  ngOnInit() {
    this.service.getData()
    .subscribe(result => this.courseList = result);
  }

  addData()
    {
      this.service.addData(this.newname, this.newjadwal);
              

      this.newname = "";
      this.newjadwal = "";
      
           
      
    }

     
  

}
