<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\mahasiswa;

class StudentController extends Controller
{
     function getData()
        {

            $siswa = mahasiswa::get(); 

            return response()->json($siswa, 200);
        }

    function addData(Request $request)
        {
            //DB transaction
            DB::beginTransaction(); 

            try
                {
                    //validasi client untuk input
                   $this->validate($request, [
                    'nama' => 'required|max:255',
                    'alamat' => 'required|max:255',
                    'telp' => 'required|max:15',
        ]);

                    //save ke database
                    $nama = $request->input('nama'); //data diambil dari client ke server
                    $alamat = $request->input('alamat');
                    $telp = $request->input('telp');

                    // save ke database menggunakan metode eloquen
                    $newSiswa = new mahasiswa;
                    $newSiswa->nama = $nama;
                    $newSiswa->alamat = $alamat;
                    $newSiswa->telp = $telp;
                    $newSiswa->save();

                     $newSiswa = mahasiswa::get();

                    DB::commit(); //jika insert data sukses maka data akan dicommit dan disave ke database
                    return response()->json($newSiswa, 201); //saat add user maka akan melakukan add $usrList
                }
            catch (\Exception $e)
                {
                    DB::rollback(); //dan jika gagal maka data akan dirollback
                    return response()->json(["message" => $e->getMessage()], 500); //500->internal server error
                }
        }

        function updateData(Request $request)
        {
            //DB transaction
            DB::beginTransaction(); 

            try
                {
                    
                  mahasiswa::where('id', $id)->update([
                        $request->all()
                ]);
                    
                    $nama = $request->input('nama'); //data diambil dari client ke server
                    $alamat = $request->input('alamat');
                    $telp = $request->input('telp');

                    // save ke database menggunakan metode eloquen
                    $newSiswa = new mahasiswa;
                    $newSiswa->nama = $nama;
                    $newSiswa->alamat = $alamat;
                    $newSiswa->telp = $telp;
                    $newSiswa->save();

                     $newSiswa = mahasiswa::get();

                    DB::commit(); //jika insert data sukses maka data akan dicommit dan disave ke database
                    return response()->json($newSiswa, 200); //saat add user maka akan melakukan add $usrList
                }
            catch (\Exception $e)
                {
                    DB::rollback(); //dan jika gagal maka data akan dirollback
                    return response()->json(["message" => $e->getMessage()], 500); //500->internal server error
                }
        }
}
