<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'mycourse'], function(){
    Route::get('/','CourseController@getData');
    Route::post('/add','CourseController@addData');
    Route::post('/update','CourseController@updateData');

    //add, get dan delete butuh middleware(harus pakai token)
});

Route::group(['prefix' => 'mystudent'], function(){
    Route::get('/','StudentController@getData');
    Route::post('/add','StudentController@addData');
    Route::post('/update','StudentController@updateData');

    //add, get dan delete butuh middleware(harus pakai token)
});
